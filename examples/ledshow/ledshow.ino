#include <thk_ledstrip_controller.h>

// Instanziiere Objekte für jeden LED-Streifen
thk_LedStripController front_strip(6,33);     
thk_LedStripController back_strip(6,37);     
thk_LedStripController left_strip(3, 35);   
thk_LedStripController rigth_strip(3, 39);  

bool state = true;
uint8_t red, green, blue;
double start;

void setup()
{
    // Initialisierung der LED-Streifen
    front_strip.init();
    back_strip.init();
    left_strip.init();
    rigth_strip.init();

    // Helligkeit ändern
    front_strip.set_brightness(150);
    back_strip.set_brightness(150);
    left_strip.set_brightness(100);
    rigth_strip.set_brightness(100);
}

void loop()
{
    start = millis();
    blink(10000); // 10 Sekunden Blinken alle LEDs gleichzeitig
    front_strip.blink(thk_LedStripController::Color(red, green, blue), 10000); // Nur der ausgewählte LED-Streifen blinkt für 10 Sekunden
    
    delay(1000); // 1 Sekunde Pause

    // Regenbogen Show wird für jeden LED-Streifen nacheinander ausgeführt
    front_strip.rainbow_effect(10);
    back_strip.rainbow_effect(10);
    left_strip.rainbow_effect(10);
    rigth_strip.rainbow_effect(10);
    
    // Ausschalten aller LED-Streifen
    front_strip.clear();
    front_strip.show();
    back_strip.clear();
    back_strip.show();
    left_strip.clear();
    left_strip.show();
    rigth_strip.clear();
    rigth_strip.show();

    delay(1000); // 1 Sekunde Pause

    start = millis();
    // Lichtlauf im ausgewähltem LED-Streifen für 5 Sekunden
    while ((millis()-start) < 5000)
    {
        front_strip.running_light(50,3, thk_LedStripController::Color(20, 0, 0),1);
        front_strip.running_light(50,3, thk_LedStripController::Color(20, 0, 0),0);    
    }
    
    delay(1000); // 1 Sekunde Pause
}

// Blink Funktion
void blink(int duration){
    while ((millis()-start) < duration)
    {
        if (state){
            // LEDs on
            red = 20;
            green = 0;
            blue = 30;
            state=false;
        }
        else {
            // LEDs off
            red = 0;
            green = 0;
            blue = 0;
            state=true;
        }
        delay(1000);

        front_strip.set_pixel_color(thk_LedStripController::Color(red, green, blue));
        front_strip.show();
        back_strip.set_pixel_color(thk_LedStripController::Color(red, green, blue));
        back_strip.show();
        left_strip.set_pixel_color(thk_LedStripController::Color(red,green, blue));
        left_strip.show();
        rigth_strip.set_pixel_color(thk_LedStripController::Color(red, green, blue));
        rigth_strip.show();
    }
}