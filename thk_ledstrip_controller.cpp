#include "thk_ledstrip_controller.h"

thk_LedStripController::thk_LedStripController(const uint16_t MAX_LED_NUMBER, const uint16_t DATA_PIN, uint8_t brightness): MAX_LED_NUMBER(MAX_LED_NUMBER), DATA_PIN(DATA_PIN), brightness(brightness)  {}

void thk_LedStripController::init() {
    ptrStrip = new Adafruit_NeoPixel(MAX_LED_NUMBER, DATA_PIN, NEO_GRB + NEO_KHZ800);
    ptrStrip->setBrightness(brightness);
    ptrStrip->begin();
    clear();
    show();
}

void thk_LedStripController::set_brightness(uint8_t brightness) {
    ptrStrip->setBrightness(brightness);
}

void thk_LedStripController::clear() {
    ptrStrip->clear();
}

void thk_LedStripController::set_pixel_color(uint32_t color) {
    set_pixel_color(0, ptrStrip->numPixels(), color);
}

void thk_LedStripController::set_pixel_color(uint16_t index, uint32_t color)
{
    ptrStrip->setPixelColor(index, color);
}

void thk_LedStripController::set_pixel_color(uint16_t start_index, uint16_t amount, uint32_t color) {
    ptrStrip->fill(color, start_index, amount);
}

uint32_t thk_LedStripController::get_pixel_color(uint16_t index) {
    uint32_t pixel_color = ptrStrip->getPixelColor(index);
    return pixel_color;
}

void thk_LedStripController::show() {
    ptrStrip->show();
}

uint32_t thk_LedStripController::color_hsv(int pixelHue) {
    pixelHue = ptrStrip->ColorHSV(pixelHue);
    return pixelHue;
}

uint32_t thk_LedStripController::gamma(int pixelHue) {
    pixelHue = ptrStrip->gamma32(color_hsv(pixelHue));
    return pixelHue;
}

void thk_LedStripController::blink(uint32_t color, unsigned long wait_time) {
    ptrStrip->clear();
    set_pixel_color(color);
    ptrStrip->show();
    delay(wait_time);
    ptrStrip->clear();
    ptrStrip->show();
}

void thk_LedStripController::rainbow_effect(unsigned long wait_time) {
    for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) 
    {
        for(uint16_t i=0; i<ptrStrip->numPixels(); i++) 
        { 
            int pixelHue = firstPixelHue + (i * 65536L / ptrStrip->numPixels());
            ptrStrip->setPixelColor(i, ptrStrip->gamma32(ptrStrip->ColorHSV(pixelHue)));
        }
        ptrStrip->show();
        delay(wait_time);
    }
}

void thk_LedStripController::running_light(unsigned long wait_time, uint8_t length, uint32_t color, bool direction) {
    if (direction){
        for(uint16_t i = 0; i <= ptrStrip->numPixels(); i++) {
            if(i <= ptrStrip->numPixels())
            {
                ptrStrip->setPixelColor(i, color);
            }
            else
            {
                ptrStrip-> setPixelColor(i - ptrStrip->numPixels(), color);
            }
            int last_pixel = i - length;

            ptrStrip->show();
            delay(wait_time);

            if (last_pixel >= 0 && last_pixel <= ptrStrip->numPixels())
            {
                ptrStrip->setPixelColor(last_pixel, Color(0, 0, 0));
            }

            ptrStrip->show();
            delay(wait_time); 
        }
        clear();
        show();
    }
    else {
        for(int8_t i = ptrStrip->numPixels(); i >= 0; i--) { 
            if(i >= 0)
            {
                ptrStrip->setPixelColor(i, color);
            }
            else
            {
                ptrStrip-> setPixelColor(i + ptrStrip->numPixels(), color);
            }
            int last_pixel = i + length;
            ptrStrip->show();
            delay(wait_time);

            if (last_pixel >= 0 && last_pixel <= ptrStrip->numPixels())
            {
                ptrStrip->setPixelColor(last_pixel, Color(0, 0, 0));
            }

            ptrStrip->show();  
            delay(wait_time);
        }
        clear();
        show();
    }
    
    
}