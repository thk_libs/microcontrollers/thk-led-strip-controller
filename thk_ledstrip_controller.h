/**
 * Klasse: 
 * 
 * 
 * 
 * 
 * 
 **/

#ifndef LEDSTRIP_H
#define LEDSTRIP_H

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif


class thk_LedStripController {
    public:
        thk_LedStripController(const uint16_t MAX_LED_NUMBER, const uint16_t DATA_PIN, uint8_t brightness = 64);
        void init();
        void clear();
        void set_brightness(uint8_t brightness);
        static uint32_t Color(uint8_t red, uint8_t green, uint8_t blue) { return Adafruit_NeoPixel::Color(red, green, blue);};
        void set_pixel_color(uint32_t color);
        void set_pixel_color(uint16_t index, uint32_t color);
        void set_pixel_color(uint16_t index, uint16_t amount, uint32_t color);
        uint32_t get_pixel_color(uint16_t index);
        void show();
        uint32_t color_hsv(int pixelHue);
        uint32_t gamma(int pixelHue);
        void blink(uint32_t color, unsigned long wait_time);
        void rainbow_effect(unsigned long wait_time);
        void running_light(unsigned long wait_time = 100, uint8_t length = 2, uint32_t color = 50, bool direction= true);
    private:
        Adafruit_NeoPixel* ptrStrip;
        const uint16_t MAX_LED_NUMBER, DATA_PIN; 
        uint8_t brightness;
};

#endif



